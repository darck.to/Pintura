(function() {
 
  $('.menu').load('templates/menu/menu.html', function(e) {
    // carga menu
    cargaPortfolio()
  });

}());

// Carga portfolio
function cargaPortfolio(e) {
    $.ajax({
        type: 'POST',
        url: 'php/portada/portada-template-carga.php',
        async: true,
        dataType : 'json',
        crossDomain: true,
        context: document.body,
        cache: false,
        success: function(data) {
          $.each(data, function (name, value) {
            if (value.success == true) {
              // Obtenemos los datos de plantilla para agregarlos a input
              $.each(value.data, function (id, datos) {
                $('.titleHolder').html(datos.title);
                $('#messageHolder').html(datos.message);
                $('#servicesHolder').html(datos.services);
                $('#addressHolder').html(datos.address)
              })
            }
          });
        },
        error: function(xhr, tst, err) {
          //console.log(err)
        }
    })
    $.ajax({
        type: 'POST',
        url: 'php/portfolio/portfolio-carga-list.php',
        async: true,
        dataType : 'json',
        crossDomain: true,
        context: document.body,
        cache: false,
        success: function(data) {
            var txt = ""
            $.each(data, function (name, value) {
            if (value.success == true) {
                $.each(value.data, function(index, content){
                txt += '<div class="column is-4 portfolio-item">';
                    txt += '<a href="entry.html?entry=' + content.id + '">';
                    txt += '<img src="assets/portfolio/img/' + content.foto + '"/>';
                    txt += '<h2 class="portfolio-title">' + content.title + '<h2/>';
                    txt += '<p class="portfolio-message">' + content.message.substring(0,35) + '<p/>';
                    txt += '</a>';
                txt += '</div>';
                })
            }
            })
            $('#portfolioHolder').html(txt)
        },
        error: function(xhr, tst, err) {
            //console.log(err)
        }
    })
  // AOs
  AOS.init()
}