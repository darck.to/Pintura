(function() {
    $('.menu').load('templates/menu/menu.html', function(e) {
        cargaTemplate()
    });

    $('.sendContact').on('click', function(e){
        var name, poc, message;
        if ($('.nameContact').val()) { name = $('.nameContact').val() } else { $('.nameContact').css('border','1px solid red'); toast('No Contact Name'); return false }
        if ($('.pocContact').val()) { poc = $('.pocContact').val()} else { $('.pocContact').css('border','1px solid red'); toast('No Contact Info'); return false }
        if ($('.messageContact').val()) { message = $('.messageContact').val()} else { $('.messageContact').css('border','1px solid red'); toast('No Message Entered'); return false }
        sendContactMessage(name, poc, message)
    })
  }())

  // Function Carga Portada
function cargaTemplate(e) {
    // Carga informacion de plantilla
    $.ajax({
      type: 'POST',
      url: 'php/portada/portada-template-carga.php',
      async: true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        $.each(data, function (name, value) {
          if (value.success == true) {
            // Obtenemos los datos de plantilla para agregarlos a input
            $.each(value.data, function (id, datos) {
              $('.titleHolder').html(datos.title);
              $('#messageHolder').html(datos.message);
              $('#servicesHolder').html(datos.services);
              $('#addressHolder').html(datos.address)
            })
          }
        });
      },
      error: function(xhr, tst, err) {
        //console.log(err)
      }
    })
    // AOs
    AOS.init()
}
  
function sendContactMessage(n,p,m) {
    var name = n;
    var poc = p;
    var message = m;
    $.ajax({
        type: 'POST',
        url: 'php/contact/contact-message-new.php',
        data: {
            action : 0,
            name : name,
            poc : poc,
            message : message
        },
        async: true,
        dataType : 'json',
        crossDomain: true,
        context: document.body,
        cache: false,
        success: function(data) {
            $.each(data, function (name, value) {
                if (value.success) {
                    $('.nameContact').val('');
                    $('.pocContact').val('');
                    $('.messageContact').val('')
                }
                toast(value.message)
            })
        },
        error: function(xhr, tst, err) {
            //console.log(err)
        }
    })
}