(function() {
    // Recibe la informacion del listado del portfolio
    cargaPorftolioList()
}());
  
function cargaPorftolioList(e) {
  var a = localStorage.getItem("rep_key");
  var u = localStorage.getItem("rep_user");
  var l = localStorage.getItem("rep_token")
  //COMPROBAMOS SI EXISTE LOGIN LOCAL
  if (l == 1 ) {
    //LOCAL LOGIN NULL OR NEGATIVE
    $.ajax({
      type: 'POST',
      url: 'php/dashboard/dashboard-portfolio-list-carga.php',
      data: {
        auth : a,
        user : u
      },
      async: true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        var txt = '';
        $.each(data, function (name, value) {
          if (value.success == true) {
            // Obtenemos los datos de plantilla para agregarlos a input
            $.each(value.data, function (id, datos) {
              txt += '<a class="panel-block">';
                txt += '<span class="panel-icon portfolioEdit" action="1" val="' + datos.id + '">';
                  if(datos.portada == 1) {
                    txt += '<i class="is-size-6 fas fa-star has-text-warning"></i>';  
                  } else {
                    txt += '<i class="is-size-6 fas fa-star"></i>';
                  }
                txt += '</span>';
                txt += '<span class="panel-icon portfolioEdit" action="2" val="' + datos.id + '" data-title="' + datos.title + '" data-message="' + datos.message + '" data-foto="' + datos.foto + '">';
                  txt += '<i class="is-size-6 fas fa-pen-to-square"></i>';
                txt += '</span>';
                txt += '<span class="panel-icon portfolioEdit" action="3" val="' + datos.id + '">';
                  txt += '<i class="is-size-6 fas fa-circle-minus"></i>';
                txt += '</span>';
                txt += '<span class="showMessages" data-id="' + datos.id + '">' + datos.title + '</span>';
              txt += '</a>'
            })
          }
        })
        $('.portfolioList').html(txt);
        // Shows messages and how to edit them
        $('.showMessages').on('click', function(e) {
          cargaMessages($(this).data('id'))
        })
        // Acciones destacar/edit/borrar
        $('.portfolioEdit').on('click', function(e) {
          if ($(this).attr('action') == 2) {
            actualizaPortfolio($(this).attr('action'), $(this).attr('val'), $(this).data('title'), $(this).data('message'), $(this).data('foto'))
          } else {
            actualizaPortfolio($(this).attr('action'), $(this).attr('val'))
          }
        })
        
      },
      error: function(xhr, tst, err) {
        toast('El tiempo de espera fue superado, por favor intentalo en un momento mas');
        modax();
      }
    })
  } else {
    toast('No se encontró el Token')
  }
}

function actualizaPortfolio(e,i,ti,me,fo) {
  var a = localStorage.getItem("rep_key");
  var u = localStorage.getItem("rep_user");

  var formData = new FormData();
  formData.append('auth', a);
  formData.append('user', u);
  formData.append('id', i);
  formData.append('action', e);

  if (e == 1) {
    doPortfolio()
  // If action 2
  } else if (e == 2) {
    // Get entry info
    var title = ti;
    var message = me;
    var foto = fo, pic
    modap()
    // Load edit info
    $('.modal-content').load('templates/dashboard/dashboard-portfolio-new.html', function(e) {
      // Bigger n better
      $('.modal-content').css('width','1010px');
      $('.txtTitle').val(title);
      $('.preTitle').html(title);
      $('.preSubTitle').html(title);
      $('.txtMessage').val(message.replace(/\\r?\\n|\\r|\\n\\n/g, "\r\n"));
      $('.preMessage').html(message.replace(/\\r?\\n|\\r|\\n\\n/g, "<br/>"));
      $('.preImgFrame').attr('src','assets/portfolio/img/' + foto);
      // Changue save message to edit
      $('#savePortfolio').remove();
      $('.entryButton').html('<button class="button" id="editPortfolio">Edit</button>');
      // Edit
      $('#editPortfolio').on('click', function(e) {
        // Reviewing img file input status
        var $foto = $(".imgLoader");
        if (parseInt($foto.get(0).files.length)){
          // True pic
          pic = document.getElementById('imgPortfolio').files[0]
        } else {
          pic = false
        }
        // Send to edit function
        formData.append('picture', pic);
        formData.append('title', $('.txtTitle').val());
        formData.append('message', $('.txtMessage').val());
        doPortfolio();
        modax()
      })
    })
  } else if (e == 3) {
    var text = '<span class="title">Are you sure you want to delete this entry?</span>';
    text += '<p><small>This action is irreversible</small></p>';
    text += '<button class="button is-warning" id="deleteEntry">Delete Entry</button>';
    modap();
    $('.modal-content .box').html(text);
    $('#deleteEntry').on('click', function(e) {
      doPortfolio();
      modax()
    })
  }
  // Go to portfolio actions
  function doPortfolio() {
    var formMethod = 'Post';
    var rutaScrtip = 'php/dashboard/dashboard-portfolio-edit.php';
  
    var request = $.ajax({
      url: rutaScrtip,
      method: formMethod,
      data: formData,
      contentType: false,
      processData: false,
      async: true,
      dataType: 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
    });
    // handle the responses
    request.done(function(data) {
      $.each(data, function (name, value) {
        toast(value.message);
        cargaPorftolioList()
      })
    })
    request.fail(function(jqXHR, textStatus) {
      console.log(textStatus);
    })
    request.always(function(data) {
      // clear the form
      //$('').trigger('reset')
    });
  }
}

// Message managment
function cargaMessages(i) {
  var id = i;
  $.ajax({
    type: 'POST',
    url: 'php/portfolio/portfolio-entry-message.php',
    data: {
      id : id,
      action : 1
    },
    async: true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      var txt = ""
      $.each(data, function (name, value) {
        if (value.success == true) {
          txt += '<span>Received messages:</span>';
          $.each(value.data, function (index, content) {
            if (content.flag == 1 || content.flag == 2) {
              txt += '<div class="message-body mb-2">';
                txt += '<span class="title is-size-5 mr-3">' + content.message.replace(/\\r?\\n|\\r|\\n\\n/g, "<br/>") + '</span>';
                txt += '<span class="subtitle is-size-7 mr-3">' + content.name + ' | ' + content.mail + '</span>';
                txt += '<span class="subtitle is-size-7">' + content.date + '</span>';
                txt += '<div class="field mt-2">';
                  txt += '<div class="control">';
                  txt += '<input class="input answerInput" type="text" placeholder="Your answer" value="' + content.answer + '" data-sub="' + content.id + '"/>';
                  txt += '</div>';
                txt += '</div>';
                txt += '<button class="button is-link answerMessage" data-sub="' + content.id + '">Send</button>';
                txt += '<button class="button is-pulled-right is-warning confirmRemove" data-sub="' + content.id + '">Delete</button>';
                txt += '<button class="button is-pulled-right is-danger removeMessage is-hidden" data-sub="' + content.id + '">Are you Sure?</button>';
              txt += '</div>'
            }
          })
          modap();
          $('.modal-content .box').html(txt);
          // Answer message adding an input into modal
          $('.answerMessage').on('click', function(e) {
            var subid = $(this).data('sub')
            var answer = $('input[data-sub="' + subid + '"]').val();
            if($('input[data-sub="' + subid + '"]').val()) {
              answerMessage(id,subid,answer)
            } else {
              $('input[data-sub="' + subid + '"]').css('border','1px solid red');
              toast('No Answer');
              return false
            }
          })
          // Removes Message
          $('.confirmRemove').on('click', function(e) {
            var subid = $(this).data('sub');
            $('.removeMessage[data-sub="' + subid + '"]').removeClass('is-hidden');
            $(this).addClass('is-hidden')
          })
          $('.removeMessage').on('click', function(e) {
            var subid = $(this).data('sub');
            removeMessage(id,subid)
          })
        }
        toast(value.message)
      })
    },
    error: function(xhr, tst, err) {
      //console.log(err)
    }
  })
  
}

function answerMessage(i,s,m) {
  var id = i;
  var subid = s;
  var answer = m
  // Send only answer
  $.ajax({
    type: 'POST',
    url: 'php/portfolio/portfolio-entry-message.php',
    data: {
      id : id,
      subid : subid,
      action : 3,
      answer : answer
    },
    async: true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $.each(data, function (name, value) {
        if (value.success == true) {
          $('input[data-sub="' + subid + '"]').addClass('is-success')
        }
        toast(value.message)
      })
    },
    error: function(xhr, tst, err) {
      //console.log(err)
    }
  })
}

// Fuction removes message
function removeMessage(i,s) {
  var id = i;
  var subid = s
  // Send only answer
  $.ajax({
    type: 'POST',
    url: 'php/portfolio/portfolio-entry-message.php',
    data: {
      id : id,
      subid : subid,
      action : 2
    },
    async: true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $.each(data, function (name, value) {
        if (value.success == true) {
          modax()
        }
        toast(value.message)
      })
    },
    error: function(xhr, tst, err) {
      //console.log(err)
    }
  })
}