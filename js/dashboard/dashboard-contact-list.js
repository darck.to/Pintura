(function() {
    // Recibe la informacion del listado de POCS
    cargaContactMessageList()
}());
  
function cargaContactMessageList(e) {
  let a = localStorage.getItem("rep_key");
  let u = localStorage.getItem("rep_user");
  let l = localStorage.getItem("rep_token")
  //COMPROBAMOS SI EXISTE LOGIN LOCAL
  if (l == 1 ) {
    //LOCAL LOGIN NULL OR NEGATIVE
    $.ajax({
      type: 'POST',
      url: 'php/dashboard/dashboard-contact-list.php',
      data: {
        action : 1,
        auth : a,
        user : u
      },
      async: true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        let txt = '';
        $.each(data, function (name, value) {
          if (value.success == true) {
            // Obtenemos los datos de plantilla para agregarlos a input
            $.each(value.data, function (id, datos) {
              // If unread
              let size = '';
              if (datos.flag == 1) { size = "is-size-7" }
              if (datos.flag == 3) { return false }
              txt += '<a class="panel-block ' + size + ' showContactMessage" data-id="' + datos.id + '" data-name="' + datos.name + '" data-poc="' + datos.poc + '" data-message="' + datos.message + '" data-date="' + datos.date + '">';
                txt += '<span class="panel-icon">';
                    if(datos.flag == 2) {
                        txt += '<i class="fa-solid fa-check has-text-link-dark"></i>';  
                    } else if (datos.flag == 1) {
                        txt += '<i class="fa-solid fa-check-double"></i>';
                    }
                txt += '</span>';
                txt += '<span>Message from: ' + datos.name + '</span>';
              txt += '</a>'
            })
            $('.contactMessagesList').html(txt)
          }
        })
        // Show contact message on message card
        $('.showContactMessage').on('click', function(e) {
            // Clean elements
            // Id file
            let id = $(this).data('id')
            let name = $(this).data('name');
            let poc = $(this).data('poc');
            let message = $(this).data('message');
            let date = $(this).data('date');
            showContactMessage(id, name, poc, message, date)
        })
      },
      error: function(xhr, tst, err) {
        toast('El tiempo de espera fue superado, por favor intentalo en un momento mas');
        modax();
      }
    })
  } else {
    toast('No se encontró el Token')
  }
}

// Carga el contenido del mensaje
function showContactMessage(i,n,p,m,d) {
    let id = i;
    let name = n;
    let poc = p;
    let message = m;
    let date = d;
    // Put on html
    $('.contactName').html(name);
    // If mail
    if (validateEmail(poc) == true) {
      poc = '<a href="mailto:' + poc + '">' + poc + '</a>'
    }
    $('.contactPoc').html(poc);
    $('.contactMessage').html(message);
    $('.contactDate').html(date);
    // Footer buttons
    let txt = '<a class="card-footer-item is-clickable editContact" data-flag="1" data-id="">File</a>';
    txt += '<a class="card-footer-item is-clickable editContact" data-flag="3" data-id="">Delete</a>';
    $('.card-footer').html(txt);
    $('.editContact').attr('data-id',id)

    // Actions from buttons and mail link if any
    $('.editContact').on('click', function(e) {
      let id = $(this).data('id');
      let flag = $(this).data('flag');
      let formData = new FormData();

      formData.append('action', 1);
      formData.append('id', id);
      formData.append('flag', flag);
      
      let formMethod = 'Post';
      let rutaScrtip = 'php/contact/contact-message-new.php';

      let request = $.ajax({
          url: rutaScrtip,
          method: formMethod,
          data: formData,
          contentType: false,
          processData: false,
          async: true,
          dataType: 'json',
          crossDomain: true,
          context: document.body,
          cache: false,
      });
      // handle the responses
      request.done(function(data) {
          $.each(data, function (name, value) {
              toast(value.message)
          })
      })
      request.fail(function(jqXHR, textStatus) {
          console.log(textStatus);
      })
      request.always(function(data) {
          // Refresh New Entry!
          $('.dashboard-body').html('');
          $('.dashboard-body').load('templates/dashboard/dashboard-contact-list.html')
      });
    });
}

// Email validation
function validateEmail(email) {
  let validation = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return validation.test(email);
}