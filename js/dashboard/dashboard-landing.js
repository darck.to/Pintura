(function() {

  // Recibe la informacion de la plantilla actual
  cargaPlantilla()

  // Carga la información a la plantilla

  $('#saveTop').on('click', function(e) {
    // Recibe lo que tenga contenido el top title y top message
    if($('#titleArea').val()) {  } else { $('#titleArea').css('border','1px solid red'); toast('No Title'); return false }
    if($('#messageArea').val()) {  } else { $('#messageArea').css('border','1px solid red');; toast('No Message'); return false }
    // Funcion para actualizar
    actualizaPlantilla('title')
  })

  $('#saveServices').on('click', function(e) {
    if($('#servicesArea').val()) {  } else { $('#servicesArea').css('border','1px solid red');; toast('No Services'); return false }
    // Funcion para actualizar
    actualizaPlantilla('services')
  })

  $('#saveAddress').on('click', function(e) {
    if($('#addressArea').val()) {  } else { $('#addressArea').css('border','1px solid red');; toast('No Address'); return false }
    // Funcion para actualizar
    actualizaPlantilla('address')
  })

}());

function cargaPlantilla(e) {
  var a = localStorage.getItem("rep_key");
  var u = localStorage.getItem("rep_user");
  var l = localStorage.getItem("rep_token")
  //COMPROBAMOS SI EXISTE LOGIN LOCAL
  if (l == 1 ) {
    //LOCAL LOGIN NULL OR NEGATIVE
    $.ajax({
      type: 'POST',
      url: 'php/dashboard/dashboard-template-carga.php',
      data: {
        auth : a,
        user : u
      },
      async: true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        $.each(data, function (name, value) {
          if (value.success == true) {
            // Obtenemos los datos de plantilla para agregarlos a input
            $.each(value.data, function (id, datos) {
              $('#titleArea').val(datos.title);
              $('#messageArea').val(datos.message);
              $('#servicesArea').val(datos.services);
              $('#addressArea').val(datos.address)
            })
          }
          toast(value.message)
        });
      },
      error: function(xhr, tst, err) {
        toast('El tiempo de espera fue superado, por favor intentalo en un momento mas');
        modax();
      }
    })
  } else {
    toast('No se encontró el Token')
  }
}

function actualizaPlantilla(componente) {
  var a = localStorage.getItem("rep_key");
  var u = localStorage.getItem("rep_user");

  var opcion = componente;
  var value  = $('#' + opcion + 'Area').val();

  var formData = new FormData();
  formData.append('auth', a);
  formData.append('user', u);
  formData.append('opc', opcion);
  formData.append('val', value);
  if (opcion == "title") {
    formData.append('mes', $('#messageArea').val())
  }
  var formMethod = 'Post';
  var rutaScrtip = 'php/dashboard/dashboard-landing-actualiza.php';

  var request = $.ajax({
    url: rutaScrtip,
    method: formMethod,
    data: formData,
    contentType: false,
    processData: false,
    async: true,
    dataType: 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
  });
  // handle the responses
  request.done(function(data) {
    $.each(data, function (name, value) {
      toast(value.message)
    })
  })
  request.fail(function(jqXHR, textStatus) {
    console.log(textStatus);
  })
  request.always(function(data) {
    // clear the form
    //$('').trigger('reset')
  });
}