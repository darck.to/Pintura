(function() {
    // Preview de imagen de Portafolio
    $('.imgLoader').change(function (e) { 
        const file = this.files[0];
        e.preventDefault();
        if (file) {
            let reader = new FileReader();
            reader.onload = function(event) {
                $('.preImgFrame').attr('src', event.target.result)
            }
            reader.readAsDataURL(file)
        }
    })

    // Carga todo como preview
    $('.preTxt').keyup(function(e) {
        if($(this).hasClass('txtTitle')) {
            $('.preTitle').html($(this).val())
            $('.preSubTitle').html($(this).val())            
        } else {
            $('.preMessage').html($(this).val())
        }
    })

    // Revisa que este tod el contenido
    $('#savePortfolio').on('click', function(e) {
        var $foto = $(".imgLoader");
        if (parseInt($foto.get(0).files.length) > 2){
            toast('Only 1 picture per entry');
            $('.file-label').css('border','1px solid red');
            return false;
        }
        if (parseInt($foto.get(0).files.length) == 0){
            toast('No Portfolio Picture');
            $('.file-label').css('border','1px solid red');
            return false;
        }
        if($('.txtTitle').val()) {  } else { $('.txtTitle').css('border','1px solid red'); toast('No Portofolio Title'); return false }
        if($('.txtMessage').val()) {  } else { $('.txtMessage').css('border','1px solid red'); toast('No Portofolio Content Text'); return false }
        // Funcion para guardar
        guardaEntry()
    })
}());

// Guarda la entrada al Portfolio
function guardaEntry(e) {
    var a = localStorage.getItem("rep_key");
    var u = localStorage.getItem("rep_user");

    var formData = new FormData();
    formData.append('auth', a);
    formData.append('user', u);
    
    formData.append("picture", document.getElementById('imgPortfolio').files[0]);
    formData.append('title', $('.txtTitle').val());
    formData.append('message', $('.txtMessage').val());
    
    var formMethod = 'Post';
    var rutaScrtip = 'php/dashboard/dashboard-portfolio-new.php';

    var request = $.ajax({
        url: rutaScrtip,
        method: formMethod,
        data: formData,
        contentType: false,
        processData: false,
        async: true,
        dataType: 'json',
        crossDomain: true,
        context: document.body,
        cache: false,
    });
    // handle the responses
    request.done(function(data) {
        $.each(data, function (name, value) {
            toast(value.message)
        })
    })
    request.fail(function(jqXHR, textStatus) {
        console.log(textStatus);
    })
    request.always(function(data) {
        // Refresh New Entry!
        $('.dashboard-body').load('templates/dashboard/dashboard-portfolio-new.html')
    });

}