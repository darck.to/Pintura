(function() {
  $('.menu').load('templates/menu/menu.html', function(e) {
    // carga menu
    cargaEntry()
  });
}())

// Function Carga Portada
function cargaEntry(e) {
  // Carga informacion de plantilla
  $.ajax({
    type: 'POST',
    url: 'php/portada/portada-template-carga.php',
    async: true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $.each(data, function (name, value) {
        if (value.success == true) {
          // Obtenemos los datos de plantilla para agregarlos a input
          $.each(value.data, function (id, datos) {
            $('.titleHolder').html(datos.title);
            $('#messageHolder').html(datos.message);
            $('#servicesHolder').html(datos.services);
            $('#addressHolder').html(datos.address)
          })
        }
      });
    },
    error: function(xhr, tst, err) {
      //console.log(err)
    }
  })
  // Carga entry
  $.urlParam = function(name){
      var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
      return results[1] || 0;
  }
  var entry = decodeURIComponent($.urlParam('entry'))
  $.ajax({
    type: 'POST',
    url: 'php/portfolio/portfolio-entry-carga.php',
    data: {
      id : entry
    },
    async: true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      var txt = ""
      $.each(data, function (name, value) {
        if (value.success == true) {
          $.each(value.data, function(index, content){
              $('title').prepend(content.title.split(' ').slice(0,2).join(' ') + ' in ');
              $('.headerTitle').html(content.title);
              $('.entryImg').attr('src','assets/portfolio/img/' + content.foto);
              $('.entryImg').attr('alt',content.title);
              $('.entryTitle').html(content.title);
              var message = content.message.replace(/\\r?\\n|\\r/g, "<br/>");
              $('.entryMessage').html(message)
          })
        }
        cargaMessagesForm(entry);
        cargaMessages(entry)
      })
    },
    error: function(xhr, tst, err) {
      //console.log(err)
    }
  })
  // AOs
  AOS.init()
}

// Messages from Entry
function cargaMessages(e) {
  var id = e;
  $.ajax({
    type: 'POST',
    url: 'php/portfolio/portfolio-entry-message.php',
    data: {
      id : id,
      action : 1
    },
    async: true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      var txt = ""
      $.each(data, function (name, value) {
        if (value.success == true) {
          txt += '<span>Received messages:</span>';
          $.each(value.data, function (index, content) {
            if (content.flag == 1 || content.flag == 2) {
              txt += '<div class="message-body mb-2">';
              txt += '<div class="p-2 m-2 has-background-white-bis br-4">';
                txt += '<span class="title is-size-5 mr-3">' + content.message.replace(/\\r?\\n|\\r|\\n/g, "<br/>") + '</span>';
                txt += '<span class="subtitle is-size-7 mr-3"><b>from: </b>' + content.name + ' | ' + content.mail + '</span>';
                txt += '<span class="subtitle is-size-7">' + content.date + '</span>';
              txt += '</div>'
              if (content.flag == 1) {
                txt += '<span class="is-block has-text-right"><b>answer:</b>&nbsp;<i>' + content.answer + '</i></span>';
              }
              txt += '</div>'
            }
          })
          $('.receivedMessages').html(txt)
        }
        //toast(value.message)
      })
    },
    error: function(xhr, tst, err) {
      //console.log(err)
    }
  })

}

// Messages Form
function cargaMessagesForm(e) {
  var id = e;
  var txt = '<div class="message-header">';
    txt += '<p>Leave a message:</p>';
  txt += '</div>';
  txt += '<div class="message-body">';
    txt += '<label class="label">Your Info</label>';
    txt += '<div class="columns mb-0">';
      txt += '<div class="column is-6">';
        txt += '<div class="field">';
          txt += '<div class="control">';
            txt += '<input class="input" type="text" name="name" id="userName" placeholder="Your Name">';
          txt += '</div>';
        txt += '</div>';
      txt += '</div>';
      txt += '<div class="column is-6">';
        txt += '<div class="field">';
          txt += '<div class="control">';
            txt += '<input class="input" type="text" name="name" id="userMail" placeholder="Your Email">';
          txt += '</div>';
        txt += '</div>';
      txt += '</div>';
    txt += '</div>                ';
    txt += '<div class="field">';
      txt += '<label class="label">Message</label>';
      txt += '<div class="control">';
        txt += '<textarea class="textarea" placeholder="Textarea" id="userMessage"></textarea>';
      txt += '</div>';
    txt += '</div>';
    txt += '<div class="field">';
      txt += '<div class="control">';
        txt += '<button class="button is-dark" id="userSend">Send</button>';
      txt += '</div>';
    txt += '</div>';
  txt += '</div>'
  $('.message').html(txt);
  
  // Send msg
  $('#userSend').on('click', function (e) {
    var name = $('#userName').val();
    var mail = $('#userMail').val();
    var message = $('#userMessage').val()
    $.ajax({
      type: 'POST',
      url: 'php/portfolio/portfolio-entry-message.php',
      data: {
        id : id,
        action : 0,
        name : name,
        mail : mail,
        message : message
      },
      async: true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        $.each(data, function (name, value) {
          if (value.success == true) {
            cargaMessages(id);
            $('#userName').val('');
            $('#userMail').val('');
            $('#userMessage').val('')
          }
          //toast(value.message)
        })
      },
      error: function(xhr, tst, err) {
        //console.log(err)
      }
    })
  })
}