(function() {
  // First load splash
  cargaSplash()
  // carga menu
  cargaMenu()
}());

function cargaSplash(e) {
  $('.splash').load('templates/portada/portada-principal.html')
  setTimeout(function(){
    if ($('.splash').length > 0) {
      $('.splash').fadeOut('fast', function() {
        $(this).remove()
      })
    }
  }, 3000)
}

// Function Carga Menu
function cargaMenu() {
  $('.menu').load('templates/menu/menu.html', function(e) {
    // Ahora la Portada
    cargaPortada();
    // Recibe la informacion del listado de POCS
    cargaFeed()
  });

  // Cargamos el feed button
  socialfeed('body');
  $('#feedButton').on('click', function(e) {
    // Cargamos un form de messages
    var txt = '<div class="columns is-paddingless">';
      txt += '<div class="column is-8 is-offset-2">';
        txt += '<p class="title is-size-3 titleStyle">We are Listening</p>';
        txt += '<p class="subtitle is-size-4 pl-5">Leave us a Review!</p>';
      txt += '</div>';
    txt += '</div>';
    txt += '<div class="columns is-paddingless">';
      txt += '<div class="column is-2">&nbsp;</div>';
      txt += '<div class="column is-2 has-text-centered">';
        txt += '<i class="fa-solid is-size-1 fa-bell has-text-link-dark"></i>';
      txt += '</div>';
      txt += '<div class="column is-6">';
        txt += '<div class="field">';
          txt += '<div class="control">';
            txt += '<input class="input nameContact" type="text" name="name" placeholder="Your name">';
          txt += '</div>';
        txt += '</div>';
        txt += '<div class="field">';
          txt += '<div class="control">';
            txt += '<input class="input pocContact" type="text" name="poc" placeholder="Mail or cellphone number">';
          txt += '</div>';
        txt += '</div>';
        txt += '<div class="field">';
          txt += '<div class="control">';
            txt += '<textarea class="textarea messageContact" name="messageContact"></textarea>';
          txt += '</div>';
        txt += '</div>';
        txt += '<div class="field">';
          txt += '<div class="control">';
            txt += '<button class="button is-info sendContact">Send</button>';
          txt += '</div>';
        txt += '</div>';
      txt += '</div>';
    txt += '</div>'; 
    modal(txt);
    // Envia
    $('.sendContact').on('click', function(e){
      var name, poc, message;
      if ($('.nameContact').val()) { name = $('.nameContact').val() } else { $('.nameContact').css('border','1px solid red'); toast('No Contact Name'); return false }
      if ($('.pocContact').val()) { poc = $('.pocContact').val()} else { $('.pocContact').css('border','1px solid red'); toast('No Contact Info'); return false }
      if ($('.messageContact').val()) { message = $('.messageContact').val()} else { $('.messageContact').css('border','1px solid red'); toast('No Message Entered'); return false }
      sendContactMessage(name, poc, message)
  })
  })
}

// Function Carga Portada
function cargaPortada(e) {
  // Carga informacion de plantilla
  $.ajax({
    type: 'POST',
    url: 'php/portada/portada-template-carga.php',
    async: true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $.each(data, function (name, value) {
        if (value.success == true) {
          // Obtenemos los datos de plantilla para agregarlos a input
          $.each(value.data, function (id, datos) {
            $('.titleHolder').html(datos.title);
            $('#messageHolder').html(datos.message);
            $('#servicesHolder').html(datos.services);
            $('#addressHolder').html(datos.address)
          })
        }
      });
    },
    error: function(xhr, tst, err) {
      //console.log(err)
    }
  })
  // Carga portfolio de portada
  $.ajax({
    type: 'POST',
    url: 'php/portada/portada-portfolio-carga.php',
    async: true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      var txt = ""
      $.each(data, function (name, value) {
        if (value.success == true) {
          $.each(value.data, function(index, content){
            txt += '<div class="column is-3 portfolio-item">';
              txt += '<a href="entry.html?entry=' + content.id + '">';
                txt += '<img src="assets/portfolio/img/' + content.foto + '"/>';
                txt += '<h2 class="portfolio-title">' + content.title + '<h2/>';
                txt += '<p class="portfolio-message">' + content.message.substring(0,35) + '<p/>';
              txt += '</a>';
            txt += '</div>';
          })
        }
      })
      $('#portfolioHolder').html(txt)
    },
    error: function(xhr, tst, err) {
      //console.log(err)
    }
  })
  // AOs
  AOS.init()
}

function sendContactMessage(n,p,m) {
  var name = n;
  var poc = p;
  var message = m;
  $.ajax({
      type: 'POST',
      url: 'php/contact/contact-message-new.php',
      data: {
          action : 2,
          name : name,
          poc : poc,
          message : message
      },
      async: true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
          $.each(data, function (name, value) {
              if (value.success) {
                  $('.nameContact').val('');
                  $('.pocContact').val('');
                  $('.messageContact').val('')
              }
              toast(value.message)
          })
          modax()
      },
      error: function(xhr, tst, err) {
          //console.log(err)
          modax()
      }
  })
}

function cargaFeed(e) {
  $.ajax({
    type: 'POST',
    url: 'php/portada/portada-contact-list.php',
    data: {
      action : 1
    },
    async: true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      let txt = '';
      txt += '<div class="column is-12 columns is-gapless feedContact">'
      $.each(data, function (name, value) {
        if (value.success == true) {
          // Obtenemos los datos de plantilla para agregarlos a input
          $.each(value.data, function (id, datos) {
            if (datos.flag == 4) {
              txt += '<div class="column is-6 has-text-left">';
                txt += '<div class="box">';
                  txt += '<div class="box-content">';
                    txt += '<h2 class="is-title has-text-left is-size-3"><i class="fa-solid fa-comment has-text-info is-size-4 p-4"></i>' + datos.name + '</h2>';
                    txt += '<h2>';
                      txt += '<p class="is-size-2">' + datos.message + '</p>';
                      txt += '<p class="is-size-7 has-text-left">' + datos.date + '</p>';
                    txt += '</h2>';
                  txt += '</div>';
                txt += '</div>';
              txt += '</div>'
            }
          })
        }
      })
      txt += '</div>';
      $('#feedPOC').append(txt);
      function animatethis(targetElement, speed) {
        var scrollWidth = $(targetElement).get(0).scrollWidth;
        var clientWidth = $(targetElement).get(0).clientWidth;
        $(targetElement).animate({ scrollLeft: scrollWidth - clientWidth },
        {
            duration: speed,
            complete: function () {
                targetElement.animate({ scrollLeft: 0 },
                {
                    duration: speed,
                    complete: function () {
                        animatethis(targetElement, speed);
                    }
                });
            }
        });
      };
      animatethis($('.feedContact'), 30000)
    },
    error: function(xhr, tst, err) {
      //toast('El tiempo de espera fue superado, por favor intentalo en un momento mas');
      modax()
    }
  })
}