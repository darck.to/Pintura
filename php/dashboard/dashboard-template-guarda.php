<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include_once('../functions/abre_conexion.php');
    include_once('../functions/functions.php');

    date_default_timezone_set("America/Mexico_City");
    $fechaActual = Date('Y-m-d H:i:s');
    $localIP = getHostByName(getHostName());

    $resultados = array();

    // "limpiamos" los campos del formulario de posibles códigos maliciosos
    $key = mysqli_real_escape_string($mysqli, $_POST['key']);
    $usr = mysqli_real_escape_string($mysqli, $_POST['usr']);
    $html = mysqli_real_escape_string($mysqli, $_POST['html']);
    $css = mysqli_real_escape_string($mysqli, $_POST['css']);

    // comprobamos que el usuario ingresado no haya sido registrado antes
    $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE nom ='".$usr."' AND init_index = '".$key."'");
    if ($sql->num_rows > 0) {
        $row = $sql->fetch_assoc();
        $sqlPerfil = $mysqli->query("SELECT id_per FROM perf_table WHERE id_usr ='".$row['id_usr']."'");
        if ($sqlPerfil->num_rows > 0) {
            $rowPerfil = $sqlPerfil->fetch_assoc();
            //ingresamos los datos a la BDD DESC_BR
            $sqlTemp = $mysqli->query("INSERT INTO temp_table (id_temp, id_perf, date) VALUES ('".$rowPerfil['id_per']."', 'Si', '".$fechaActual."')");
            if($sqlTemp) {
                $resultados[] = array("success"=> true, "type"=> "profile", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "Se guardó en la BDD");
            } else {
                $resultados[] = array("success"=> false, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "No se pudo guardar en al BDD".mysqli_error($mysqli));
            }
        } else {
            $resultados[] = array("success"=> false, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "No existe el perfil");
        }
    } else {
        $resultados[] = array("success"=> false, "type"=> "auth", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "El usuario no esta registrado");
    }

    print json_encode($resultados);
    // incluimos el archivo de desconexion a la Base de Datos
    include('../functions/cierra_conexion.php');
?>
