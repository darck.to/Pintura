<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    ini_set('track_errors', 1);
    include_once('../functions/abre_conexion.php');
    include_once('../functions/functions.php');

    date_default_timezone_set("America/Mexico_City");
    $fechaActual = Date('Y-m-d H:i:s');
    $localIP = getHostByName(getHostName());

    $resultados = array();

    // "limpiamos" los campos del formulario de posibles códigos maliciosos
    $auth = mysqli_real_escape_string($mysqli, $_POST['auth']);
    $user = mysqli_real_escape_string($mysqli, $_POST['user']);
    $opc = mysqli_real_escape_string($mysqli, $_POST['opc']);
    $val = mysqli_real_escape_string($mysqli, $_POST['val']);
    // Si recibimos tambien el mensaje
    if (isset($_POST['mes'])) { $mes = mysqli_real_escape_string($mysqli, $_POST['mes']); }
    
    // comprobamos que el usuario ingresado no haya sido registrado antes
    $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE nom ='".$user."' AND init_index = '".$auth."'");
    if ($sql->num_rows > 0) {
        $row = $sql->fetch_assoc();
        $sqlPerfil = $mysqli->query("SELECT id_per FROM perf_table WHERE id_usr ='".$row['id_usr']."'");
        if ($sqlPerfil->num_rows > 0) {
            $rowPerfil = $sqlPerfil->fetch_assoc();
            // Leemos los datos del archivo plantilla
            $idPerfil = $rowPerfil['id_per'];
            $file = '../../assets/template/' . $idPerfil . '.json';
            if(file_exists($file)) {
                $fileTemplate = file_get_contents($file);
                $data = json_decode($fileTemplate, true);
                foreach ($data as &$value) {
                    if ($value['id'] == 0) {
                        $value[$opc] = str_replace("\\","", $val);
                        // Si es message
                        if (isset($_POST['mes'])) {
                            $value["message"] = str_replace("\\","", $mes);
                        }
                    }
                }
                // Guardamos el archivo
                if (is_writable($file)) {
                   if (!$json = fopen($file, 'w')) {
                        $resultados[] = array("success"=> false, "type"=> "read file", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "No se pudo abrir el archivo: " . $file . $php_errormsg);
                    }
                    if (fwrite($json, json_encode($data, JSON_PRETTY_PRINT))) {
                        $resultados[] = array("success"=> true, "type"=> "read file", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "Se guardo la plantilla en archivo");
                    } else {
                        $resultados[] = array("success"=> false, "type"=> "read file", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "No se pudo guardar el archivo: " . $file . $php_errormsg);
                    }
                    fclose($json);
                } else {
                    $resultados[] = array("success"=> false, "type"=> "read file", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "No se puede escribir en el archivo: " . $file . $php_errormsg);
                }
            } else {
                $resultados[] = array("success"=> false, "type"=> "read file", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "No se pudo leér la plantilla: " . $file);
            }
        } else {
            $resultados[] = array("success"=> false, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "No existe el perfil");
        }
    } else {
        $resultados[] = array("success"=> false, "type"=> "auth", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "El usuario no esta registrado");
    }

    print json_encode($resultados);
    // incluimos el archivo de desconexion a la Base de Datos
    include('../functions/cierra_conexion.php');
?>
