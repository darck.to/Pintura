<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include_once('../functions/abre_conexion.php');
    include_once('../functions/functions.php');

    date_default_timezone_set("America/Mexico_City");
    $fechaActual = Date('Y-m-d H:i:s');
    $localIP = getHostByName(getHostName());

    $random = generateRandomString(8);

    $resultados = array();

    // "limpiamos" los campos del formulario de posibles códigos maliciosos
    $auth = mysqli_real_escape_string($mysqli, $_POST['auth']);
    $user = mysqli_real_escape_string($mysqli, $_POST['user']);

    $id = mysqli_real_escape_string($mysqli, $_POST['id']);
    $action = mysqli_real_escape_string($mysqli, $_POST['action']);

    // File
    $file = $id . '.json';
    
    // comprobamos que el usuario ingresado no haya sido registrado antes
    $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE nom ='".$user."' AND init_index = '".$auth."'");
    if ($sql->num_rows > 0) {
        // Mover a Portada
        if ($action == 1) {
            $jsonString = file_get_contents('../../assets/portfolio/data/' . $file);
            $data = json_decode($jsonString, true);
            $viejo = $data[0]['portada'];
            //LEÉMOS EL VALOR Y LO CAMBIAMOS EN CONTRA
            if ($data[0]['portada'] == 1) {
                $portada = 0;
            } elseif ($data[0]['portada'] == 0) {
                $portada = 1;
            }
            $data[0]['portada'] = $portada;
            //LO VOLVEMOS A GUARDAR
            $newJsonString = json_encode($data, JSON_PRETTY_PRINT);
            if (file_put_contents('../../assets/portfolio/data/' . $file, $newJsonString)) {
                $resultados[] = array("success"=> true, "message"=> "Modified");
            } else {
                $resultados[] = array("success"=> false, "message"=> "Error, contact support");
            }
        //EDITAR NOTA
        } elseif ($action == 2) {
            //RECIBIMOS LA INFORMACION
            $title = mysqli_real_escape_string($mysqli, $_POST['title']);
            $message = mysqli_real_escape_string($mysqli, $_POST['message']);
            //INICIALIZAMOS INDICADORES EN FALSE
            $foto = false;
            //GUARDAR IMAGENES EN CARPETA
            if (isset($_FILES['picture'])) {
                $errors = array();
                $file_tmp = $_FILES['picture']['tmp_name'];
                $file_type = $_FILES['picture']['type'];
                $file_ext = 'PNG';
                //CAMBIAMOS EL NOMBRE DEL ARCHIVO
                $file_name = "img_" . $random . "." . $file_ext;
                $extensions = array("PNG");
                if (in_array($file_ext,$extensions) === false) {
                    $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                }
                if (empty($errors) == true) {
                    //LE CAMBIAMOS EL TAMAÑO A LA IMAGEN ANTES DE GUARDARLA EN LA BDD
                    $file_name_final = "../../assets/portfolio/img/" . $file_name;
                    $source_img = $file_tmp;
                    $destination_img = $file_name_final;
                    //LLAMAMOS LA FUNCION PARA CAMBIARLE EL TAMAÑO
                    $file_compressed = compress($source_img, $destination_img, 99);
                    //LA GUARDAMOS EN EL DIRECTORIO CORRESPONDIENTE
                    move_uploaded_file($file_compressed,"." . $file_name);
                    $resultados[] = array("success"=> true, "message"=> "image Edited");
                    $foto = true;
                } else {
                    $resultados[] = array("success"=> false, "message"=> "Image couldn't be edited, contact support " . $errors);
                }
            } else {
                $resultados[] = array("success"=> true, "message"=> "Image couldn't be edited, contact support");
            }
            //GUARDAMOS EL ARCHIVO DE LA ENTRADA
            $fileName = '../../assets/portfolio/data/' . $file;
            //MODIFICAMOS
            $jsonString = file_get_contents('../../assets/portfolio/data/' . $file);
            $data = json_decode($jsonString, true);
            //LEÉMOS EL VALOR Y LO CAMBIAMOS EN CONTRA
            $data[0]['modified'] = $fechaActual;
            $data[0]['title'] = $title;
            $data[0]['message'] = $message;
            //SI SE EDITA IMAGENES, SE GUARDAN IMAGENES
            if ($foto == true) {
                $data[0]['foto'] = $file_name;
            }
            //LO VOLVEMOS A GUARDAR
            $newJsonString = json_encode($data, JSON_PRETTY_PRINT);
            if (file_put_contents('../../assets/portfolio/data/' . $file, $newJsonString)) {
                $resultados[] = array("success"=> true, "message"=> "Entry edited");
            } else {
                $resultados[] = array("success"=> false, "message"=> "Couldn't edit the entry");
            }
        //ELIMINAR BOLETIN
        } elseif ($action == 3) {
            if (unlink('../../assets/portfolio/data/' . $file)) {
                $resultados[] = array("success"=> true, "message"=> "Entry deleted");
            } else {
                $resultados[] = array("success"=> false, "message"=> "Couldn't delete entry");
            }
        }
    } else {
        $resultados[] = array("success"=> false, "type"=> "auth", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "No user!");
    }

    print json_encode($resultados);
    // incluimos el archivo de desconexion a la Base de Datos
    include('../functions/cierra_conexion.php');
?>
