<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include_once('../functions/abre_conexion.php');
    include_once('../functions/functions.php');

    date_default_timezone_set("America/Mexico_City");
    $fechaActual = Date('Y-m-d H:i:s');
    $localIP = getHostByName(getHostName());

    $random = generateRandomString(8);

    $resultados = array();

    // "limpiamos" los campos del formulario de posibles códigos maliciosos
    $auth = mysqli_real_escape_string($mysqli, $_POST['auth']);
    $user = mysqli_real_escape_string($mysqli, $_POST['user']);
    $title = mysqli_real_escape_string($mysqli, $_POST['title']);
    $message = mysqli_real_escape_string($mysqli, $_POST['message']);
    
    // comprobamos que el usuario ingresado no haya sido registrado antes
    $sql = $mysqli->query("SELECT id_usr FROM auth_table WHERE nom ='".$user."' AND init_index = '".$auth."'");
    if ($sql->num_rows > 0) {
        //GUARDAR IMAGENES EN CARPETA
        if (isset($_FILES['picture'])) {
            $errors = array();
            $file_tmp = $_FILES['picture']['tmp_name'];
            $file_type = $_FILES['picture']['type'];
            $file_ext = 'PNG';
            //CAMBIAMOS EL NOMBRE DEL ARCHIVO A UNO MAS CORTO Y DE ACUERDO A LA NOMENCLATURA DEL IDATT
            $file_name = "img_" . $random . "." . $file_ext;
            $extensions= array("PNG");
            if (in_array($file_ext,$extensions) === false) {
                $errors[]="extension not allowed, please choose a JPEG or PNG file.";
            }
            if (empty($errors) == true) {
                //LE CAMBIAMOS EL TAMAÑO A LA IMAGEN ANTES DE GUARDARLA EN LA BDD
                $file_name_final = "../../assets/portfolio/img/" . $file_name;
                $source_img = $file_tmp;
                $destination_img = $file_name_final;
                //LLAMAMOS LA FUNCION PARA CAMBIARLE EL TAMAÑO
                $file_compressed = compress($source_img, $destination_img, 99);
                //LA GUARDAMOS EN EL DIRECTORIO CORRESPONDIENTE
                move_uploaded_file($file_compressed," . " . $file_name);
                $resultados[] = array("success"=> true, "message"=> "Se Subio Imagen: ". $file_name_final);
            } else {
                //print_r($errors);
                $resultados[] = array("success"=> false, "message"=> "No se puedo subir la imagen, consulta a soporte " . $errors);
            }
        } else {
            //echo "Ocurrio un error, favor de consultar al administrador";
            $resultados[] = array("success"=> false, "message"=> "No se puedo subir la imagen, consulta a soporte " . $errors);
            $file_name_final = "";
        }
        // Guardamos la entrada el portfolio
        $cabecera[] = array('id'=> $random, 'creation'=> $fechaActual, 'modified'=> $fechaActual, 'title'=> $title, 'message'=> $message, 'foto'=> $file_name, 'portada'=> 0);
        // Nombre el archivos
        $file = '../../assets/portfolio/data/' . $random . '.json';
        if(!file_exists($file)) {
            if ($json = fopen($file, 'w')) {
                if (fwrite($json, json_encode($cabecera, JSON_PRETTY_PRINT))) {
                    $resultados[] = array("success"=> true, "type"=> "read file", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "Se guardo la plantilla en archivo");
                    fclose($json);
                    chmod($file, 0777);
                }
            } else {
                $resultados[] = array("success"=> false, "type"=> "read file", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "No se pudo guardar el archivo: " . $file . $php_errormsg);
            }
        } else {
            $resultados[] = array("success"=> false, "type"=> "read file", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "No se pudo leér la plantilla: " . $file);
        }
    } else {
        $resultados[] = array("success"=> false, "type"=> "auth", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "El usuario no esta registrado");
    }

    print json_encode($resultados);
    // incluimos el archivo de desconexion a la Base de Datos
    include('../functions/cierra_conexion.php');
?>
