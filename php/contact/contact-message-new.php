<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include_once('../functions/abre_conexion.php');
    include_once('../functions/functions.php');

    date_default_timezone_set("America/Mexico_City");
    $fechaActual = Date('Y-m-d H:i:s');
    $localIP = getHostByName(getHostName());

    $action = mysqli_real_escape_string($mysqli, $_POST['action']);

    $random = generateRandomString(12);

    // File name
    $file = '../../assets/contact/message_' . $random . '.json';
    
    // Create from Entry
    if ($action == 0) {
        // "limpiamos" los campos del formulario de posibles códigos maliciosos
        $name = mysqli_real_escape_string($mysqli, $_POST['name']);
        $poc = mysqli_real_escape_string($mysqli, $_POST['poc']);
        $message = mysqli_real_escape_string($mysqli, $_POST['message']);
        
        // Add to file
        if (!file_exists($file)) {
            // Data to save/add
            $cabecera[] = array('id'=> $random, 'flag'=> 2, 'name'=> $name, 'poc'=> $poc, 'message'=> $message, 'date'=> $fechaActual, 'answer'=>'');
            if ($json = fopen($file, 'w')) {
                if (fwrite($json, json_encode($cabecera, JSON_PRETTY_PRINT))) {
                    fclose($json);
                    chmod($file, 0777);
                    $resultados[] = array("success"=> true, "type"=> "read file", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "Message Sent!");
                }
            } else {
                $resultados[] = array("success"=> false, "type"=> "read file", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "Couldn't send the message: " . $php_errormsg);
            }
        }
    // Edit or delete
    } elseif ($action == 1) {
        $id = mysqli_real_escape_string($mysqli, $_POST['id']);
        $file = '../../assets/contact/message_' . $id . '.json';
        $flag = mysqli_real_escape_string($mysqli, $_POST['flag']);
        if (file_exists($file)) {
            $fileJson = file_get_contents($file);
            $json = json_decode($fileJson, true);
            foreach($json as &$content) {
                $content['flag'] = $flag;
            }
            if ($filejson = fopen($file, 'w')) {
                if (fwrite($filejson, json_encode($json, JSON_PRETTY_PRINT))) {
                    $resultados[] = array("success"=> true, "message"=> "Message modified!");
                } else {
                    $resultados[] = array("success"=> false, "message"=> "Couldn't remove the message");
                }
                fclose($filejson);
                chmod($file, 0777);
            }
        } else {
            $resultados[] = array("success"=> false, "type"=> "auth", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "Could't file message");
        }
    } elseif ($action == 2) {
        // "limpiamos" los campos del formulario de posibles códigos maliciosos
        $name = mysqli_real_escape_string($mysqli, $_POST['name']);
        $poc = mysqli_real_escape_string($mysqli, $_POST['poc']);
        $message = mysqli_real_escape_string($mysqli, $_POST['message']);
        
        // Add to file
        if (!file_exists($file)) {
            // Data to save/add
            $cabecera[] = array('id'=> $random, 'flag'=> 4, 'name'=> $name, 'poc'=> $poc, 'message'=> $message, 'date'=> $fechaActual, 'answer'=>'');
            if ($json = fopen($file, 'w')) {
                if (fwrite($json, json_encode($cabecera, JSON_PRETTY_PRINT))) {
                    fclose($json);
                    chmod($file, 0777);
                    $resultados[] = array("success"=> true, "type"=> "read file", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "Message Sent!");
                }
            } else {
                $resultados[] = array("success"=> false, "type"=> "read file", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "Couldn't send the message: " . $php_errormsg);
            }
        }
    } else {
        $resultados[] = array("success"=> false, "type"=> "auth", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "Couldn't deliver the message");
    }

    print json_encode($resultados);
    include_once('../functions/cierra_conexion.php');
?>