<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include_once('../functions/abre_conexion.php');
    include_once('../functions/functions.php');

    date_default_timezone_set("America/Mexico_City");
    $fechaActual = Date('Y-m-d H:i:s');
    $localIP = getHostByName(getHostName());

    $random = generateRandomString(9);

    $resultados = array();
    

    // "limpiamos" los campos del formulario de posibles códigos maliciosos
    $id = mysqli_real_escape_string($mysqli, $_POST['id']);
    $action = mysqli_real_escape_string($mysqli, $_POST['action']);
    
    
    // File name
    $file = '../../assets/portfolio/msg/' . $id . '.json';
    
    // Create from Entry
    if ($action == 0) {
        $name = mysqli_real_escape_string($mysqli, $_POST['name']);
        $mail = mysqli_real_escape_string($mysqli, $_POST['mail']);
        $message = mysqli_real_escape_string($mysqli, $_POST['message']);
        // Add to file
        if (file_exists($file)) {
            // Data to save/add
            $cabecera = array('id'=> $random, 'flag'=> 2, 'name'=> $name, 'mail'=> $mail, 'message'=> $message, 'date'=> $fechaActual, 'answer'=>'');
            $content = file_get_contents($file);
            $tmp = json_decode($content, true);
            array_push($tmp, $cabecera);
            // Overwrites it
            $new = json_encode($tmp, JSON_PRETTY_PRINT);
            if (file_put_contents($file, $new)) {
                $resultados[] = array("success"=> true, "message"=> "Message Added!: ");
            } else {
                $resultados[] = array("success"=> false, "message"=> "Couldn't add the message");
            }
            // Create file
        } else {
            // Data to save/add
            $cabecera[] = array('id'=> $random, 'flag'=> 2, 'name'=> $name, 'mail'=> $mail, 'message'=> $message, 'date'=> $fechaActual, 'answer'=>'');
            if ($json = fopen($file, 'w')) {
                if (fwrite($json, json_encode($cabecera, JSON_PRETTY_PRINT))) {
                    fclose($json);
                    chmod($file, 0777);
                    $resultados[] = array("success"=> true, "type"=> "read file", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "Message Send!");
                }
            } else {
                $resultados[] = array("success"=> false, "type"=> "read file", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "Couldn't send the message: " . $php_errormsg);
            }
        }
    // Read from Entry
    } elseif ($action == 1) {
        if (file_exists($file)) {
            $file = file_get_contents($file);
            $json = json_decode($file, true);
            $resultados[] = array("success"=> true, "message"=>"Messages", "data"=> $json);
        } else {
            $resultados[] = array("success"=> false, "type"=> "auth", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "Missing file");
        }
    // Delete message
    } elseif ($action == 2) {
        $subid = mysqli_real_escape_string($mysqli, $_POST['subid']);
        if (file_exists($file)) {
            $fileJson = file_get_contents($file);
            $json = json_decode($fileJson, true);
            foreach($json as &$content) {
                if($content['id'] == $subid) {
                    $content['flag'] = 3;
                }
            }
            if ($filejson = fopen($file, 'w')) {
                if (fwrite($filejson, json_encode($json, JSON_PRETTY_PRINT))) {
                    $resultados[] = array("success"=> true, "message"=> "Message deleted!");
                } else {
                    $resultados[] = array("success"=> false, "message"=> "Couldn't remove the message");
                }
                fclose($filejson);
                chmod($file, 0777);
            }
        } else {
            $resultados[] = array("success"=> false, "type"=> "auth", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "Could't remove message");
        }
    // Answer message from Dashboard
    } elseif ($action == 3) {
        $subid = mysqli_real_escape_string($mysqli, $_POST['subid']);
        $answer = mysqli_real_escape_string($mysqli, $_POST['answer']);
        if (file_exists($file)) {
            $fileJson = file_get_contents($file);
            $json = json_decode($fileJson, true);
            foreach($json as &$content) {
                if ($subid == $content['id']) {
                    $content['flag'] = 1;
                    $content['answer'] = $answer;
                }
            }
            if ($filejson = fopen($file, 'w')) {
                if (fwrite($filejson, json_encode($json, JSON_PRETTY_PRINT))) {
                    $resultados[] = array("success"=> true, "message"=> "Message answered!");
                } else {
                    $resultados[] = array("success"=> false, "message"=> "Couldn't add the answer to file");
                }
                fclose($filejson);
                chmod($file, 0777);
            }
        } else {
            $resultados[] = array("success"=> false, "type"=> "auth", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "Could't answer message");
        }
    }

    print json_encode($resultados);
    // incluimos el archivo de desconexion a la Base de Datos
    include('../functions/cierra_conexion.php');
?>