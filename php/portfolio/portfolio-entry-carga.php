<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include_once('../functions/abre_conexion.php');

    date_default_timezone_set("America/Mexico_City");
    $fechaActual = Date('Y-m-d H:i:s');
    $localIP = getHostByName(getHostName());

    $resultados = array();

    // "limpiamos" los campos del formulario de posibles códigos maliciosos
    $id = mysqli_real_escape_string($mysqli, $_POST['id']);
    
    //NOMBRE DE ARCHIVO
    $file = '../../assets/portfolio/data/' . $id . '.json';
    if (file_exists($file)) {
        $file = file_get_contents($file);
        $json = json_decode($file, true);
        $resultados[] = array("success"=> true, "data"=> $json);
    } else {
        $resultados[] = array("success"=> false, "type"=> "auth", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "El archivo esta dañado");        
    }

    print json_encode($resultados);
    // incluimos el archivo de desconexion a la Base de Datos
    include('../functions/cierra_conexion.php');
?>
