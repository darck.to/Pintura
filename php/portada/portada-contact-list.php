<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include_once('../functions/abre_conexion.php');
    include_once('../functions/functions.php');

    date_default_timezone_set("America/Mexico_City");
    $fechaActual = Date('Y-m-d H:i:s');
    $localIP = getHostByName(getHostName());

    $resultados = array();

    //NOMBRE DE ARCHIVO
    $fileList = glob('../../assets/contact/*.json');
    //ORDENAMOS EL ARREGLO DE ARCHIVOS POR FECHA
    natsort($fileList);
    $fileList = array_reverse($fileList, false);
    //RECORREMOS LOS ARCHIVOS
    foreach($fileList as $filename){
        //SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
        if (file_exists($filename)) {
            $filename = file_get_contents($filename);
            $json = json_decode($filename, true);
            // Busca POCS de portada
            foreach ($json as $value) {
                if ($value["flag"] == 4) {
                    $resultados[] = array("success"=> true, "data"=> $json);
                }
            }
        } else {
            $resultados[] = array("success"=> false, "type"=> "auth", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "El archivo esta dañado");        
        }
    }

    print json_encode($resultados);
    // incluimos el archivo de desconexion a la Base de Datos
    include('../functions/cierra_conexion.php');
?>