<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');

    date_default_timezone_set("America/Mexico_City");
    $fechaActual = Date('Y-m-d H:i:s');
    $localIP = getHostByName(getHostName());

    $resultados = array();
    
    // Leemos los datos del archivo plantilla
    $file = '../../assets/template/SbbLn1JN.json';
    if(file_exists($file)) {
        $fileTemplate = file_get_contents($file);
        $data = json_decode($fileTemplate, true);
        $resultados[] = array("success"=> true, "type"=> "read file", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "Se leyó la plantilla", "data"=> $data);
    } else {
        $resultados[] = array("success"=> false, "type"=> "read file", "ip"=> $localIP, "date"=> $fechaActual, "message"=> "No se pudo leér la plantilla: " . $file);
    }

    print json_encode($resultados);
?>
